package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

@WebServlet(urlPatterns = {"/borrarCookie"})
public class BorrrarCookie extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9019877098698445103L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nombreCookie = req.getParameter("cookie");
		
		
		Cookie[] cookies = req.getCookies();
		System.out.println("Parametroooooooo: "+nombreCookie);
		for(Cookie cookie : cookies){
		if(cookie.getName().equals(nombreCookie)){
			   Cookie cook = new Cookie(nombreCookie, "");
		        cook.setMaxAge(0);
		        resp.addCookie(cook);
		        resp.getWriter().println("se ha borrado la cookie!");
		    
			System.out.println("cookies encontradas: "+cookie.getName());
		}
		else{
			resp.getWriter().println("no se ha encontrado la cookie");
			
		}
	}
}		

}
