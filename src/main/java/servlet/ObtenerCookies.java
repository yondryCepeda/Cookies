package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/obtenerCookie"})
public class ObtenerCookies extends HttpServlet{
	private String nombre = "nombre"; 
	/**
	 * 
	 */
	private static final long serialVersionUID = 3514619762694061030L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	String nombre = "",valor="";
	
		
		
		nombre = req.getParameter("nombre");
		valor = req.getParameter("valor"); 
		
		resp.addCookie(new Cookie(nombre, valor));
		resp.getWriter().println("se ha agregado la cookie!");
		
		
	}

	
}
